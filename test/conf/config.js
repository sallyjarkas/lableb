let HtmlReporter = require('protractor-beautiful-reporter');
exports.config = {
    allScriptsTimeout: 12000,
    framework: 'jasmine',
    directConnect: true,
    // seleniumAddress: 'http://localhost:4444/wd/hub',
    SELENIUM_PROMISE_MANAGER: false,

    capabilities: {
        'browserName': 'chrome',
        chromeOptions: {
            'args': ['show-fps-counter=true', 'disable-extensions', 'start-maximized']
        }
    },

    jasmineNodeOpts: {
        defaultTimeoutInterval: 360000,  // Default time to wait in ms before a test fails.
        includeStackTrace: true,
        // isVerbose: true,
    },

    onPrepare: function () {
        browser.driver.manage().window().maximize();
        browser.manage().timeouts().implicitlyWait(30000);
        global.EC = protractor.ExpectedConditions;

        // global.;

        global.publicURL = "https://solutions.lableb.com/";
        browser.waitForAngularEnabled(false);
        jasmine.getEnv().addReporter(new HtmlReporter({
            baseDirectory: 'test/report',
            screenshotsSubfolder: 'images',
            jsonsSubfolder: 'jsons',
            takeScreenShotsForSkippedSpecs: true,
            docTitle: 'Lableb Testing Report',
            consolidate: false,
            consolidateAll: false,
            cleanDestination: true,
            // clear destination
            preserveDirectory: false,
            fileName: 'Lableb Report',
            clientDefaults: {
                showTotalDurationIn: "header",
                totalDurationFormat: "hms",
                columnSettings: {
                    displayBrowser: false,
                    displaySessionId: false,
                    displayOS: false,
                    warningTime: 40000,
                    dangerTime: 60000
                }
            }
        }).getJasmine2Reporter());
    },
    suites: {
        Login: [
            '../test_spec/login.js',
        ],

    },

};