const property = require('../resources/properties/adminPage.js');

var LoginPagePO = function () {
    let loginHomePage = element(by.xpath(property.loginHomePage));
    this.clickLoginHomePage = async (pass) => {
        await loginHomePage.click();
    };
    let emailAddress = element(by.id(property.user.email));
    this.setEmail = async (email) => {
        await emailAddress.sendKeys(email);
    };

    let password = element(by.id(property.user.password));
    this.setPassword = async (pass) => {
        await password.sendKeys(pass);
    };

    let loginButton = element(by.xpath(property.user.loginButton));
    this.clickLogin = async (pass) => {
        await loginButton.click();
    };

    };

module.exports = new LoginPagePO();