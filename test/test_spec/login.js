describe('Login', function () {

    const login = require('../page_objects/LoginPagePO');


    const data = require('../resources/data/sharedData/loginData.json');
    
    it('login with admin credentials ', async () => {
            await browser.get(publicURL + "login");
            await login.clickLoginHomePage();
            await login.setEmail(data.credentials.email);
            await login.setPassword(data.credentials.password);
            await login.clickLogin();

    });
});