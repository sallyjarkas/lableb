# Lableb

# Introduction 
This is a simple web automated test script using PROTRACTOR Framework and it is built considering the POM model

# Code Structure
- conf folder: is where the system test configured including reports, test suites, etc.
- page_objects: here we represent each page (Login page) as an object to make the code readable and maintainable 
- resources: contains the data folder where we add the test data to apply data driven ( in our example we use json file) and the properties file is where to save the locators of DOM elements
- test_spec: where the actual test cases are.
ore)